extends Node2D

onready var poly := $Polygon2D as Polygon2D
onready var line := $Line2D as Line2D

func _ready() -> void:
	print("%.20f" % (0.1+0.2))
	var p := poly.polygon
	var point := line.get_points()
	var global : PoolVector2Array = poly.get_global_transform().xform(p)
	
	var new := Geometry.clip_polyline_with_polygon_2d(point, global)
	for i in new.size():
		var colors : PoolStringArray = ["6b66ff", "66ff85", "feff66", "fe66ff"]
		var _l := line.duplicate() as Line2D
		_l.set_points(new[i] as PoolVector2Array)
		_l.set_default_color( Color( colors[i] ) )
		add_child(_l)
