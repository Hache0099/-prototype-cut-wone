extends Node2D

onready var _anim := $AnimationPlayer as AnimationPlayer

func _ready() -> void:
	pass

func cut() -> void:
	_anim.play("hand_rotate")


func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	pass # Replace with function body.
