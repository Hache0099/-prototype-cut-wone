extends Node2D

onready var line2d := $Line2D as Line2D
onready var FalseLine := $Line2D/FalseLine as Line2D
onready var raycast := $Line2D/RayCast2D as RayCast2D

export (float, 10, 500) var inner_circle_r :float= 50.0
export (float, 10, 500) var outter_circle_r :float= 100.0
export (float, 1, 200) var vertical_radius := 50.0

export (float, 10, 180) var angle_spread := 45.0
export (float, 0, 360) var initial_angle := 0.0
export (float, 1, 500) var line_speed := 1.0
export (float, 50, 500) var raycast_lenght := 250.0
export (bool) var is_drunk := true

var points_angle := 0.0
var _noise := OpenSimplexNoise.new()

func _ready() -> void:
	randomize()
	_set_noise()
	_set_line_points()
	line2d.rotation_degrees = rand_range(-angle_spread, angle_spread)
	
	
	
	raycast.position = line2d.get_point_position(0)
	raycast.set_cast_to(Vector2.RIGHT * raycast_lenght)


func _set_noise()->void:
	_noise.seed = randi()
	_noise.octaves = 4
	_noise.period = 10.0
	_noise.persistence = 1.0


func _get_noise_value(idx : float) -> float:
	return _noise.get_noise_1d(idx)


func _set_line_points() -> void:
	var rad_array : PoolRealArray = [inner_circle_r, outter_circle_r]
	var arr := line2d.get_points()
	
	for i in arr.size():
		var _i :int= i as int
		var s : float = rad_array[i] as float
		line2d.set_point_position(_i, Vector2.RIGHT * s)

#cos(deg2rad(points_angle))
func _process(delta: float) -> void:
	points_angle += line_speed * delta
	_line_rotate() if !is_drunk else _line_rotate_random()


func _line_rotate_random() -> void:
	var rand := _get_noise_value( deg2rad(points_angle) )
	line2d.rotation_degrees = rand * angle_spread
	line2d.position.y = vertical_radius * rand


func _line_rotate():
	var rot := cos(deg2rad(points_angle))
	line2d.rotation_degrees = rot * angle_spread
	line2d.position.y = rot * vertical_radius


func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_accept"):
		stop_and_get_line()


func stop_and_get_line() -> PoolVector2Array:
	set_process(false)
	raycast.set_enabled(true)
	raycast.force_raycast_update()

	if !raycast.is_colliding():
		return PoolVector2Array([])

	var trans := FalseLine.get_global_transform()
	var arr := FalseLine.get_points()
	var new_points : PoolVector2Array = trans.xform(arr)
	
	return new_points
