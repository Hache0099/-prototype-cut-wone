class_name SpriteSlicer
extends Node2D


func offset_polygon_points(polygon : PoolVector2Array,
							offset : Vector2)->PoolVector2Array:
	var new_poly : PoolVector2Array = []
	
	for i in polygon.size():
		var vec := polygon[i]
		new_poly.append(vec + offset)
	
	return new_poly


func rotate_polygon_points(polygon : PoolVector2Array, angle : float, 
							offset : Vector2 = Vector2.ZERO) -> PoolVector2Array:
	var pol : PoolVector2Array = Transform2D(angle, offset).xform(polygon)
	return pol


func get_points_relative(polygon_node : Polygon2D,
						points : PoolVector2Array) -> PoolVector2Array:
	var new_points : PoolVector2Array = []
	
	for i in points.size():
		new_points.append(points[i] - polygon_node.position)
	
	return new_points


func get_from_polygon(polygon : Polygon2D, line_cut : PoolVector2Array) -> Array:
	var nodes_array := []
	var points := polygon.polygon
	assert(points.size() > 0, "polygon is empty")
	assert(line_cut.size() == 2, 
			"line_cut size must be 2, but is %d instead" % line_cut.size())
	
	
	var polygon_transform := polygon.get_transform()
	polygon_transform.origin = Vector2.ZERO
	
	var inverse := polygon_transform.affine_inverse()
	
	var transform_points : PoolVector2Array = polygon_transform.xform(points)
	var local_line := get_points_relative(polygon, line_cut)
	
	var _poly := set_polygon(transform_points, local_line)

	assert(_poly.size() > 0, "No polygons were created")
	
	for i in _poly.size():
		var _points := _poly[i] as PoolVector2Array
		if _points:
			var p := Polygon2D.new()
			p.texture = polygon.get_texture()
			p.set_polygon(_points)
			p.uv = inverse.xform(_points)
			nodes_array.append(p)
	
	return nodes_array


func set_polygon(polygon : PoolVector2Array, line_cut : PoolVector2Array) -> Array:
	assert(polygon.size() > 0, "polygon is empty")
	assert(line_cut.size() == 2, "line_cut size must be 2, but is %d instead" % line_cut.size())
	
	
	var _new_points := _get_new_points(polygon, line_cut)
	if _new_points.empty():
		return []
	
	var _calculate := _calculate_points_in_polygon(polygon, _new_points)
	var _add_points := _add_points_to_polygon(polygon, _calculate[0], _calculate[1])
	var new_polygons := _separate_polygon(_add_points[0], _add_points[1])
	
	return new_polygons


func _get_new_points(polygon : PoolVector2Array,
			line : PoolVector2Array,
			accuracy : int = 200) -> PoolVector2Array:
	var _delta : float = 0.0
	var _variation : float = 1.0 / float(accuracy)
	var interp_vec : Vector2 = Vector2()
	var cutted_line : PoolVector2Array = []
	
	
	while _delta < 1.0:
		interp_vec = line[0].linear_interpolate(line[1], _delta)
		
		if Geometry.is_point_in_polygon(interp_vec, polygon):
			cutted_line.append(interp_vec)
		
		_delta += _variation

	if cutted_line.empty():
		return cutted_line

	var s1 := cutted_line[0]
	var s2 := cutted_line[cutted_line.size() - 1]
	var new_points : PoolVector2Array = [s1, s2]
	
	return new_points


func _calculate_points_in_polygon(polygon : PoolVector2Array, 
								points : PoolVector2Array)-> Array:
	var min_distance : float = -1.0
	var min_distance_line : Array = []
	var new_points : Array = []
	
	
	for j in points.size():
		min_distance = -1.0
		min_distance_line.append([])
		new_points.append(null)
		
		for i in polygon.size():
			var point_vec : Vector2 = points[j]
			var s1 : Vector2 = polygon[i]
			var s2 : Vector2 = polygon[(i + 1) % polygon.size()]
			var arr : Vector2 = Geometry.get_closest_point_to_segment_2d(point_vec,s1,s2)
			var dist : float = arr.distance_to(point_vec)
			
			if dist < min_distance or min_distance == -1.0:
				min_distance_line[j] = [i, (i + 1) % polygon.size()]
				min_distance = dist
				new_points[j] = arr
	
	return [min_distance_line, new_points]


func _add_points_to_polygon(_polygon : PoolVector2Array,
							line_index : Array, _line_points : Array) -> Array:
	var pol : PoolVector2Array = _polygon
	var old_max : int = pol.size()
	var last_line := [old_max - 1, 0]
	var line_idxs : Array = []

	var is_bigger : bool = line_index[0].max() > line_index[1].max()
	var first_is_last : bool = line_index[0] == last_line
	
	if is_bigger or first_is_last:
		line_index.invert()
		_line_points.invert()
	
	for i in line_index.size():
		var _max = line_index[i].max()
		var _l = line_index[i]
		
		match _l:
			last_line:
				pol.append(_line_points[i])
				line_idxs.append(pol.size() - 1)
			_:
# warning-ignore:return_value_discarded
				pol.insert(_max + i, _line_points[i])
				line_idxs.append(_max + i)

	return [pol, line_idxs]


func _separate_polygon(_polygon : PoolVector2Array, _line_cut : Array) -> Array:
	var idx : int = 0
	var divided := [[], []]
	var points := [[], []]

	while idx <= _polygon.size() - 1:
		var vec := _polygon[idx]
		points[0].append(vec)
		divided[0].append(idx)
		
		if idx == _line_cut[0]:
			idx = _line_cut[1]
			continue
		else:
			idx += 1
	
	for i in _polygon.size():
		if !divided[0].has(i) or _line_cut.has(i):
			var vec := _polygon[i]
			divided[1].append(i)
			points[1].append(vec)

	var pool_points := []
	for i in points.size():
		var pool := PoolVector2Array(points[i])
		pool_points.append(pool)
	
	return pool_points
