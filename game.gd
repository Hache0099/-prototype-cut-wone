extends Node2D

onready var LineCut := $LineCut
onready var line2d := $Line2D as Line2D
onready var wone := $Polygon2D as Polygon2D
var test_line : PoolVector2Array = []

func _ready() -> void:
	test_line.resize(0)

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_accept"):
		var li : PoolVector2Array = LineCut.stop_and_get_line()
#		test_line = li
#		set_line(li)
		if !li.empty():
			cut_wonejas(li)


	if event.is_action_pressed("ui_cancel"):
		get_tree().reload_current_scene()


func cut_wonejas(line : PoolVector2Array) -> void:
	var slicer := SpriteSlicer.new()

	var arr := slicer.get_from_polygon(wone, line)
	
	for poly in arr:
		poly.position = wone.position
		$new_polygons.add_child(poly)
	wone.hide()
	$samurai.cut()
	

func set_line(line : PoolVector2Array) -> void:
	for i in line.size():
		line2d.add_point(line[i])


func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	var arr := $new_polygons.get_children()
	for i in arr.size():
		var _poly := arr[i] as Polygon2D
		var offset := -pow(-1.0, i)
		var new_pos := _poly.position.x + 20.0 * offset
		if _poly:
			$Tween.interpolate_property(_poly,"position:x", null, new_pos, 
			0.5,Tween.TRANS_QUAD,Tween.EASE_OUT)
	
	$Tween.start()
